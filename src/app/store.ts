import { INCREMENT } from './actions';
import { tassign } from 'tassign';
import { Map } from 'immutable';

export interface IAppState {
    counter: number;
}

export const INITIAL_STATE: IAppState = {
    counter: 0
}

export function rootReducer(state: Map<string, any>, action): Map<string, any> {
    switch (action.type) {
        case INCREMENT: 
            let incrementedState = state.set('counter', state.get('counter') + 1);
            //return Object.assign({}, state, incrementedState);
            //return tassign(state, incrementedState); // typesafe and immutable version of objects merging function
            return incrementedState;
        default: 
            return state;
    }
}