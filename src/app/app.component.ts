import { Component } from '@angular/core';
import { NgRedux, select } from 'ng2-redux';
import { IAppState } from './store';
import { INCREMENT } from './actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';
  //@select() counter; // picks observeable property from a state
  @select(state => state.get('counter')) counter; 
  /*
    @select('counter') count; // pick by name
    @select(['messaging', 'newMessages']) newMessages; // subproperty
    @select((state: IAppState) => state.messaging.newMessages) newMessagesCount; // subproperty
  */

  constructor(private ngRedux: NgRedux<IAppState>) {
    
  }

  increment() {
    //this.counter++;
    let incrementAction = { type: INCREMENT };
    this.ngRedux.dispatch(incrementAction);
  }

}
